let page = 1;
let isLoading = false;

function fetchData() {
    isLoading = true;
    const apiUrl = `https://flipkart-scraper-api.dvishal485.workers.dev/search/smartwatch?page=${page}`;
    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            const smartwatchList = document.getElementById("smartwatch-list");
            data.result.forEach(item => {
                const card = document.createElement("div");
                card.classList.add("col-md-4", "mb-3");
                card.innerHTML = `
                    <div class="card">
                        <img src="${item.thumbnail}" class="card-img-top" alt="${item.name}">
                        <div class="card-body">
                            <h5 class="card-title">${item.name}</h5>
                            <p class="card-text">Price: $${item.current_price}</p>
                            <a href="${item.link}" class="btn btn-primary" target="_blank">View Details</a>
                        </div>
                    </div>
                `;
                smartwatchList.appendChild(card);
            });
            page++;
            isLoading = false;
        })
        .catch(error => {
            console.error("Error fetching data: ", error);
        });
}

fetchData();

window.addEventListener("scroll", () => {
    if (isLoading) return;

    const scrollable = document.documentElement.scrollHeight - window.innerHeight;
    console.log(window.innerHeight);
    console.log(document.documentElement.scrollHeight);
    console.log(window.scrollY);
    const scrolled = window.scrollY;

    if (scrolled >= scrollable - 100) {
      console.log(window.scrollY,"done");
        fetchData();
    }
});
